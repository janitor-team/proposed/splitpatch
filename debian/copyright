Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: splitpatch.rb
Source: https://github.com/jaalto/splitpatch
Comment: Original author is <peter.hutterer@who-t.net>.
         The Co-maintainers are <benjamin.Close@ClearChain.com> and
         Jari Aalto <jari.aalto@cante.net>.
         .
         The original homepage and source is at
         http://www.clearchain.com/blog/posts/splitting-a-patch
         and https://github.com/benjsc/splitpatch
         .
         The latest source code is at
         https://github.com/jaalto/splitpatch

Files: *
Copyright: 2007-2014 Benjamin Close <Benjamin.Close@clearchain.com>
           2007-2014 Peter Hutterer <peter.hutterer@who-t.net>
           2008-2014 Jari Aalto <jari.aalto@cante.net>
License: GPL-2+

Files: debian/*
Copyright: 2013-2016 Jari Aalto <jari.aalto@cante.net>
           2019      Joao Eriberto Mota Filho <eriberto@debian.org>
           2019      Thiago Andrade Marques <thmarques@gmail.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".
